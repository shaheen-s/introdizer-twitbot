import time
from os.path import abspath
from packages.introdizer.internet_scraper import InternetScraper
from packages.introdizer.rewriter import Rewriter
import datetime
from threading import Thread
import json
from twython import Twython


APP_KEY = 'gud6e7daT0aDqmUkF27Dm67RM'
APP_SECRET = 'i0y9MrCH9QHZgFRtZROW9M4aYGkWgza8XTUY92y2UpRDLSTkwr'
OAUTH_TOKEN = '1124623145854541826-ICm5r6oTubTmvn0p6kYhMsfHTifFo7'
OAUTH_TOKEN_SECRET = 'tssh4oBRFwgNG447J599Lzx14goQnzEh0OeOgnILJzTBp'

twitter = Twython(APP_KEY, APP_SECRET, OAUTH_TOKEN, OAUTH_TOKEN_SECRET)



def new_mention_found():
    """
    Function will check if a new tweet mentioning the bot is found.
    """

    with open("resources/last_mention.json", "r") as f:
        last_mention = json.load(f)

    mentions = twitter.get_mentions_timeline()

    if mentions[0]["id"] > last_mention["id"]:

        with open("resources/last_mention.json", "w") as f:
            json.dump(mentions[0], f, indent=2)

        return mentions[0]

    return None

def introdizer_runner(topic):
    """
    This function will use the functions provided by the introdizer package
    to search for a topic and rewrite some possible introductions
    """

    geckodriver = abspath("../resources/geckodriver")
    reader = InternetScraper(geckodriver, topic)

    reader.initial_search()
    reader.site_visitor()

    rewritten_introductions = []

    for introduction in reader.introductions:
        rewriter = Rewriter(geckodriver, introduction)
        rewriter.input_paragraph()
        rewriter.bypass_question()
        rewriter.submit_request()
        rewritten_introductions.append(rewriter.get_result())

    return rewritten_introductions

def reply_to_mention():
    """
    Function will handle replying to tweets mentioning it.
    eg: '@BOTNAME hello'
    """

    while(True):

        time.sleep(10)

        last_mention = new_mention_found()

        if last_mention is not None:
            # user_mention = "@" + last_mention["user"]["screen_name"] + " "
            user_mention = last_mention["user"]["screen_name"]

            if "introduce" in last_mention["text"]:
                twitter.send_direct_message("Working...")
                user_tweet = last_mention["text"].split()
                topic = user_tweet[2:]
                introductions = introdizer_runner(topic)
                for introduction in introductions:
                    twitter.send_direct_message(introduction, user_mention)


def daily_tweet():
    """
    Function that would check for a time and post a daily tweet.
    """
    tweet_hour = 17
    while(True):
        if datetime.datetime.now().hour == tweet_hour:
            # prepare tweet
            prepared_tweet = None
            tweet = twitter.update_status(status=prepared_tweet)
            with open("resources/last_tweet.json", "w+") as f:
                json.dump(tweet, f, indent=2)

    time.sleep(1200)


replyThread = Thread(target=reply_to_mention)
# replyThread.start()
