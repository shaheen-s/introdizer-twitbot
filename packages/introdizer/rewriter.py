"""
This module will encompass the Rewriter class.
"""

from selenium import webdriver

class Rewriter:
    """
    This class will handle rewriting a paragraph of text
    temporarily, it will use articlerewriter website.
    """

    def __init__(self, driver: str, paragraph: str):

        self.driver = webdriver.Firefox(executable_path=driver)
        self.driver.get("https://articlerewritertool.com/")
        self.paragraph = paragraph
        self.text_fields = self.driver.find_elements_by_tag_name("textarea")


    def input_paragraph(self):
        """Simply inputs the paragraph"""

        self.text_fields[0].send_keys(self.paragraph)

    def find_question(self):
        """This will find the math question on the website"""

        span_elems = self.driver.find_elements_by_tag_name("span")

        for span in span_elems:
            if "=" in span.text:
                question = span
                break

        return question.text

    def bypass_question(self):
        """This function will bypass the mathematical question on the website"""

        question = self.find_question().split()

        number_text = {"one": 1, "two": 2, "three": 3, "four": 4, "five": 5,
                       "six": 6, "seven": 7, "eight": 8, "nine": 9, "ten": 10}

        try:
            first_value = int(question[0])
        except ValueError:
            first_value = number_text[question[0]]

        try:
            second_value = int(question[2])
        except ValueError:
            second_value = number_text[question[2]]


        if question[1] == "plus" or question[1] == "+":
            answer = first_value + second_value
        else:
            answer = first_value - second_value

        input_field = self.driver.find_element_by_id("math_captcha_answer")
        input_field.send_keys(str(answer))


    def submit_request(self):
        """Finds the submit button and clicks it"""

        button = self.driver.find_elements_by_class_name("button")[0]

        button.click()

    def get_result(self):
        """Returns the result of the operation"""
        self.text_fields = self.driver.find_elements_by_tag_name("textarea")
        return self.text_fields[1].text

    def __del__(self):

        self.driver.close()
