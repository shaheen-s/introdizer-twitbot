"""
This module will provide the Internet Scraper class
"""

from time import sleep
import selenium
from selenium import webdriver
from selenium.webdriver.common.keys import Keys

class InternetScraper:
    """
    This class will encompass all the necessary functions to visit the
    websites, grab their introductions and display them to the user
    """


    def __init__(self, driver: str, topic: str):

        self.driver = webdriver.Firefox(executable_path=driver)
        self.driver.get("https://www.google.com")
        self.topic = topic
        self.results = []
        self.introductions = []
        self.non_useful_words = ["what", "how", "why", "the", "of", "is", "are"]

    def initial_search(self):
        """Begins initial search for the topic"""
        search_bars = self.driver.find_elements_by_tag_name("input")
        for search_bar in search_bars:
        # This finds the search bar on the Google homepage
            try:
                search_bar.send_keys("test")
                for _ in range(4):
                    search_bar.send_keys(Keys.BACK_SPACE)
                search_bars = search_bar
                break
            except selenium.common.exceptions.ElementNotInteractableException:
                pass

        search_bars.send_keys(self.topic)
        search_bars.send_keys(Keys.RETURN)
        sleep(0.75)

    def filter_topic(self):
        """Removes unnecessary words from topic"""

        filtered_topic = set(self.topic.split())
        for word in self.non_useful_words:
            if word in filtered_topic:
                filtered_topic.remove(word)

        return filtered_topic

    def site_visitor(self):
        """Visits a website and grabs the introduction"""

        if "google.com" not in self.driver.current_url:
            raise Exception("Error: Not in a google search result")


        for i in range(4):

            results = self.driver.find_elements_by_class_name("LC20lb")
            results[i].click()
            sleep(0.75)
            introduction = self.introduction_grabber()

            if introduction is not None:
                self.introductions.append(introduction)

            self.driver.back()

    def introduction_grabber(self):
        """ Grabs the introduction paragraph"""

        paragraphs = self.driver.find_elements_by_tag_name("p")

        i = 0
        for paragraph in paragraphs:
            i += 1
            if i > 20:
                return None

            paragraph_set = set(paragraph.text.split())
            if len(self.filter_topic() & paragraph_set) > 1:
                return paragraph.text

            return None

    def display_introductions(self):
        """ Prints Introductions list"""
        print(len(self.introductions))
        for i in range(len(self.introductions)):
            try:
                print("\n\n["+str(i+1)+"]"+"\n"+self.introductions[i])
            except TypeError:
                pass

    def __del__(self):
        self.driver.close()
